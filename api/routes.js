const Router = require('koa-router');
const controllers = require('./controllers');

module.exports = ()=>{
    const router = new Router();
    router.get('/user', controllers.getUser);
    router.post('/user', controllers.createUser);
    return router;
}