const crypto = require('crypto');

class User {
    static async getUser(login, password) {
        const [ row ] = await db.any(`SELECT * FROM users WHERE login = '${login}'` );
        if(!row || User.encryptPassword(password, row.salt) !== row.hashed_password) return null;
        return new User({
            id: row.id,
            login: row.login,
            firstName: row.first_name,
            lastName: row.last_name,
        })
    }

    static async existsUser(login) {
        const [ row ] = await db.any(`SELECT * FROM users WHERE login = '${login}'` );
        return !!row;
    }

    static encryptPassword(password, salt) {
        return crypto.createHmac('sha1', salt).update(password).digest('hex');
    };

    constructor(user) {
        this._id = user.id;
        this._login = user.login,
        this._hashedPassword = null;
        this._salt = null;
        this._firstName = user.firstName || '';
        this._lastName = user.lastName || '';
    }

    setPassword(password) {
        this._salt = crypto.randomBytes(32).toString('base64');
        this._hashedPassword = User.encryptPassword(password, this._salt);
    }

    async create() {
        if(!this._login || !this._hashedPassword || !this._salt) {
            console.error('Login and password required');
            return null;
        }
        return await db.any(`INSERT INTO users (login, hashed_password, salt, first_name, last_name)
            VALUES ('${this._login}', '${this._hashedPassword}', '${this._salt}', '${this._firstName}', '${this._lastName}')`);
    }

    toJSON() {
        return {
            id: this._id,
            login: this._login,
            firstName: this._firstName,
            lastName: this._lastName
        }
    }
}

module.exports = User;