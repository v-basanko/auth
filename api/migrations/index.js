module.exports = async (db) => {
    try{
        await db.any(`CREATE TABLE IF NOT EXISTS users (
            id SERIAL,
            login varchar(50) UNIQUE NOT NULL,
            hashed_password varchar NOT NULL,
            salt varchar NOT NULL,
            first_name varchar,
            last_name varchar,
            PRIMARY KEY (id)
        )`);

        console.log("CRATED TABLE USERS");
    } catch(ex) {
        console.log('CREATE TABLES ERROR: ', ex);
        process.exit(0);
    }
};