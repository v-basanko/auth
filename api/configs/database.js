module.exports = {
      postgres: {
            user: process.env.PGUSER,
            password: process.env.PGPASSWORD,
            database: process.env.PGDATABASE,
            host: process.env.PGHOST,
            port: process.env.PGPORT
      }
};