const Koa = require('koa');
const router = require('./routes')();
const pgp = require('pg-promise')();
const migration = require('./migrations');
const { postgres } = require('./configs/database');
const app = new Koa();
global.db = pgp(postgres);

app.use(router.routes());

const startApp = async ()=>{
    await migration(db);
    app.listen(3000);
}

startApp();