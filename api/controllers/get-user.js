const { User } = require('../models');

module.exports = async (ctx)=>{
    const query = ctx.request.query;
    if(!query.login || !query.password) {
        ctx.body = 'Login and password is required';
        return;
    }
    const user = await User.getUser(query.login, query.password);
    if(!user) {
        ctx.body = 'User not found';
        return;
    }
    ctx.body = JSON.stringify(user);
}