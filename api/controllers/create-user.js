const { User } = require('../models');

const readPostData = (ctx)=>{
    return new Promise((resolve)=>{
        let data = '';
        ctx.req.on('data', chunk=>data+=chunk);
        ctx.req.on('end', async ()=>{
            resolve(data);
        });
        ctx.req.on('error', err=>{ 
            reject(err);
        });
    });
}

module.exports = async (ctx)=>{
    let data;
    try {
        const json = await readPostData(ctx);
        data = JSON.parse(json);
        if(!data.login || !data.password) throw 'Login and password is required';
        const existsUser = await User.existsUser(data.login);
        if(existsUser) throw 'User already exists';
        const user = new User({
            login: data.login,
            firstName: data.firstName,
            lastName: data.lastName,
        });
        user.setPassword(data.password);
        await user.create();
        ctx.status = 200;
        ctx.body = 'User created';
    }catch(err) {
        console.error(err);
        ctx.status = 500;
        ctx.body = 'Error';
    }
}